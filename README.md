# Keyword Filter

Here is a brief description on the Keyword Filter component

## Source Files

These files are hosted [here](https://bitbucket.org/shansurat/keyword-filter/).

## Directory Structure

```
json >
    availableColors.json
    availableFilters.json
    configuration.json
    keywordList.json
index.html
jquery-3.4.0.min.js
main.js
README.md
style.css
```

The three most crucial files here are the ```index.html```, ```main.js```, and ```style.css```. The two Javascript files - ```main.js``` and the JQuery file, and the one CSS file - ```style.css``` are all imported into the ```index.html```.

```index.html > html > head```
```
<link rel="stylesheet" href="style.css">
<script src="jquery-3.4.0.min.js"></script>
<script src="main.js"></script>
```
Looking at the ```json``` directory, that is precisely the JSON files you provided. But from those eight json files, I only use those four.

## Demonstration
You can try the keyword filter [here](https://keyword-filter.000webhostapp.com).

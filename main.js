let $configuration
let $keywordList
let $availableColors
let $availableFilters

$(() => {
    // Loading the JSON files

    $.get('json/availableColors.json', availableColors => loadKeywordListActiveColors(availableColors))
    $.get('json/availableFilters.json', availableFilters => {
        $availableFilters = availableFilters.Filter
        loadKeywordListFilters($availableFilters)
    })
    $.get('json/configuration.json', configuration => {
        loadKeywordListConfiguration(configuration)
        $.get('json/keywordList.json', keywordList => {
            $keywordList = keywordList.WordList
            activateKeywordListSort($configuration.KeywordListSortType)
        })
    })

    // Element event handlers

    // Keyword Display Option click handler
    // Either All or Active
    $('.keywordListDisplayOption').click((e) => {
        if (!$('#' + e.target.id).hasClass('keywordListDisplayActivated')) {
            $('.keywordListDisplayActivated, .keywordListDisplayNotActivated').toggleClass('keywordListDisplayActivated keywordListDisplayNotActivated')
            activateKeywordListDisplay(e.target.id == 'keywordList_activator_ALL' ? 'All' : 'Active')
        }
    })

    // Filter title click handler
    $('#filters__wrapper>.title').click(() => {
        $('#filters__wrapper').toggleClass('filters-enabled filters-disabled')

        $('#filters__wrapper').hasClass('filters-disabled') ?
            $('.activated').toggleClass('activated not_activated') :
            reloadFilters()

        hideTimestampToggle($('#filters__wrapper').hasClass('filters-disabled'))


        $('#filters__wrapper>.list>div>div>span.filter_activator').css('cursor', $('#filters__wrapper').hasClass('filters-disabled') ? 'not-allowed' : 'inherit')
    })

    // Filter list collapser
    $('#filters__wrapper>.toggle').click(() => collapseFilterList($('#filters__wrapper>.toggle').html() == '▲'))

    // Keyword List Sorter Option
    // Either Alphabetical or Numerical
    $('.keywordListSorterOption').click((e) => {
        if (!$('#' + e.target.id).hasClass('keywordListSorterActivated')) {
            $('.keywordListSorterActivated, .keywordListSorterNotActivated').toggleClass('keywordListSorterActivated keywordListSorterNotActivated')
            activateKeywordListSort(e.target.id == 'keywordList_sorter__ALPHABETICAL' ? 'Alphabetical' : 'Numerical')
        }
    })

    // Keyword List collapser
    $('#keywordList_sorter__wrapper>.toggle').click((e) => {
        collapseWordList($(e.target).html() == '▲')
        $(e.target).html($(e.target).html() == '▲' ? '▼' : '▲')
    })
})

// Keyword List loader
function loadKeywordList(keywordList) {
    $('#keyword_list__wrapper').hide('fast')
    $('#keyword_list__wrapper').empty()

    // Loading the keywords
    keywordList.forEach((keyword) => {
        let timestamp_list_node = $('<div/>', {
            class: 'timestamp_list hidden'
        })

        keyword.Occurrences.forEach((occurrence, index) => {
            timestamp_list_node.append($('<div/>', {
                    class: 'timestamp timestampNotSelected'
                }).html(
                    (index + 1) + ' - ' + convertSecondsToPageTimeDisplay(occurrence.SecondsStart) + ' - ' + convertSecondsToPageTimeDisplay(occurrence.SecondsEnd)
                )
                .css('cursor', 'pointer'))
        })

        $('#keyword_list__wrapper').append(
            $('<div/>', {
                id: 'keyword_' + keyword.Id + '_' + keyword.Content.toUpperCase().charAt(0),
                class: (keyword.Selected ? 'selected' : 'not_selected') + ' letter_' + keyword.Content.toUpperCase().charAt(0)
            })
            .css({
                'padding': '5px 0',
                color: keyword.Color

            })
            .append($('<div/>', {
                    class: 'color_palette_activator'
                })
                .css({
                    display: 'inline-block',
                    width: '13px',
                    height: '13px',
                    'background-color': keyword.Color,
                    'cursor': 'pointer',
                    'text-align': 'center',
                    'color': '#fff',
                    'font-size': '13px',
                    'line-height': '13px',
                    'font-weight': 'bolder'
                }))
            .append($('<span/>', {
                    class: 'timestamp_toggle'
                })
                .html('▼')
                .css({
                    'font-size': '16px',
                    cursor: 'pointer',
                    'padding-left': '5px'
                }))
            .append($('<span/>', {
                    class: 'content'
                })
                .html(keyword.Content)
                .css({
                    'padding': '0 5px',
                    cursor: 'pointer'
                }))

            .append($('<span/>')
                .html(keyword.Count)
                .css({
                    'float': 'right'
                }))
            .append(timestamp_list_node)
        )
    })

    // Filtering the keywords (if filter is enabled)
    reloadFilters()

    // Either All or Active
    activateKeywordListDisplay($configuration.KeywordListDisplayType)

    $('#keyword_list__wrapper').show('fast')

    // Keyword click handler
    $('#keyword_list__wrapper>div>span.content').click((e) => {
        let keyword = $(e.target).parent()
        let timestamp_toggle = keyword.find('.timestamp_toggle')

        if ($('#filters__wrapper>.toggle').text() != '+' && $('#filters__wrapper').hasClass('filters-enabled')) {
            keyword.toggleClass('activated not_activated')

            let id = keyword.attr('id').split('_')[1]

            if (!$availableFilters.find(filter => filter.Active).words)
                $availableFilters.find(filter => filter.Active).words = []

            if (keyword.hasClass('activated'))
                $availableFilters.find(filter => filter.Active).words.push({
                    Content: keyword.find('.content').text(),
                    Id: id
                })
            else {
                let words = $availableFilters.find(filter => filter.Active).words

                $availableFilters.find(filter => filter.Active).words = words.filter(word => word.Id != id)
            }

        } else {
            timestamp_toggle.html('▼')

            keyword.toggleClass('selected not_selected')


            if (keyword.find('.timestamp_list').hasClass('shown'))
                keyword.find('.timestamp_list').toggleClass('shown hidden')

            $keywordList.find((word) => word.Id == keyword.attr('id').split('_')[1]).Selected = keyword.find('.timestamp_list').hasClass('shown')
        }

        if ($configuration.KeywordListDisplayType == 'Active' && !$configuration.KeywordFilterViewCollapse) {
            keyword.hide('fast')

            let letter = keyword.find('.content').text().charAt(0).toUpperCase()
            if (!$('.letter_' + letter + '.activated').length)
                $('.letter_toggle').each((e, el) => {
                    if ($(el).children().first().text() == letter)
                        $(el).hide()
                })
        }
    })

    // Timestamp toggle click handler
    $('.timestamp_toggle').click((e) => {
        let timestamp_list__open = $(e.target).html() == '▲'

        $(e.target).html(timestamp_list__open ? '▼' : '▲')

        $(e.target).parent().find('.timestamp_list').toggleClass('hidden shown')
    })

    // Timestamp click handler
    $('.timestamp').click((e) => {
        onWordTimestampClick($(e.target).parents(':eq(1)').find('.content').text(), $(e.target).text())

        if ($(e.target).hasClass('timestampNotSelected'))

            $(e.target).parent().find('.timestampSelected').toggleClass('timestampSelected timestampNotSelected')

        $(e.target).toggleClass('timestampSelected timestampNotSelected')
    })


    // Color palette activator hover handler
    $('#keyword_list__wrapper>div>div.color_palette_activator').hover(
        e => $(e.target).html('+').css('background-color', '#000'),
        e => $(e.target).html('').css('background-color', $keywordList.find(keyword => keyword.Id == $(e.target).parent().attr('id').split('_')[1]).Color)
    )

    // Color palette activator click handler
    $('.color_palette_activator').click((e) => {
        $('#color_palette')
            .css({
                left: '10%',
                top: $(e.target).offset().top - 150
            })
        if (!$(e.target).hasClass('color_palette__activated')) {
            $('.color_palette__activated').toggleClass('color_palette__activated')
            $('#color_palette')
                .show('fast')

        } else
            $('#color_palette').hide('fast')

        $(e.target).toggleClass('color_palette__activated')

        $(e.target).html('+').css('background-color', '#000').show('fast')
    })

}

// Color palette loader
function loadKeywordListActiveColors(availableColors) {
    $availableColors = availableColors.AvailableColors
    let color_palette = $('#color_palette')

    $availableColors.forEach((color) => {
        color_palette.append($('<div/>', {
            class: 'color'
        }).css({
            'background-color': color.HexValue,
        }))
    })

    $('.color')
        // Color option hover handler
        .hover(
            e => $(e.target).text('+'),
            e => $(e.target).text('')
        )
        // Color option click handler
        .click((e) => {
            let color = $(e.target).css('background-color')
            let keyword = $('.color_palette__activated').parent()
            let keyword_id = keyword.attr('id').split('_')[1]

            $keywordList.find(word => word.Id == keyword_id).Color = color
            keyword.css('color', color)
            $('.color_palette__activated').css('background-color', color)
        })

}


// Filter list loader
function loadKeywordListFilters(availableFilters) {
    let filters_list = $('#filters__wrapper>.list')

    $availableFilters.forEach(availableFilter => {
        let name = availableFilter.FilterName
        let id = availableFilter.FilterId

        let filter =
            $('<div/>', {
                'id': 'filter_' + id
            })
            .append($('<span/>', {
                'class': 'filter_activator'
            }).html('&#9632;'))
            .append($('<span/>', {
                    class: 'name'
                })
                .text(name ? name : 'Filter ' + id)
            )
            .addClass(availableFilter.Active ? 'active' : 'inactive')

        if (id < 5)
            $(filters_list).find('.col1').append(filter)
        else if (id < 9)
            $(filters_list).find('.col2').append(filter)
        else
            $(filters_list).find('.col3').append(filter)
    })


    reloadFilters()

    // Filter activator (the bullet at the left side of every filter) click handler
    $('#filters__wrapper>.list>div>div>.filter_activator').click((e) => {
        $('#filters__wrapper>.list>div>div.active, #' + $(e.target).parent().attr('id')).toggleClass('active inactive')
        if ($availableFilters.find(filter => filter.Active))
            $availableFilters.find(filter => filter.Active).Active = false

        $availableFilters
            .find(filter => filter.FilterId == $(e.target).parent().attr('id').split('_')[1])
            .Active = $('#' + $(e.target).parent().attr('id')).hasClass('active')

        reloadFilters()
        activateKeywordListDisplay($configuration.KeywordListDisplayType)
    })

    // Filter double click handler
    $('#filters__wrapper>.list>div>div>span.name').dblclick((e) => {
        $('input.filter_editor').parent().find('.name').show()
        $('input.filter_editor').remove()
        enableFilterEditor(e, true)
    })

}

// Filter editor loader where 'e' is the filter, and 'yes' is a boolean variable to enable or disable
function enableFilterEditor(e, yes) {
    if (yes) {
        let name = $(e.target).text()

        $(e.target).hide()

        $(e.target).parent().append($('<input/>')
            .val(name)
            .addClass('filter_editor').css({
                'color': $(e.target).parent().css('color'),
                'border-color': $(e.target).parent().css('color')
            })
            .keyup((event) => {
                if (event.key == 'Enter') {
                    let name = $(event.target).val().trim()

                    $(e.target).text(name)

                    $availableFilters.find(filter => filter.FilterId == $(event.target).parent().attr('id').split('_')[1]).FilterName = name

                    enableFilterEditor(e, false)
                }
            }))
    } else {
        $(e.target).show()
        $(e.target).parent().find('.filter_editor').remove()
    }
}

// Refresh the keyword list using the current filters
function reloadFilters() {
    $('#keyword_list__wrapper>div').removeClass('activated').addClass('not_activated')
    let filters = $availableFilters.find(filter => filter.Active)
    if (filters && filters.words) {
        filters.words.forEach(filter => {
            $('#keyword_' + filter.Id + '_' + filter.Content.charAt(0).toUpperCase()).addClass('activated').removeClass('not_activated').show()
        })
    }

}

// Keyword list configuration loader
function loadKeywordListConfiguration(configuration) {
    $configuration = configuration.KeywordListConfiguration

    $('#keywordList_activator_ALL').addClass($configuration.KeywordListDisplayType == 'All' ? 'keywordListDisplayActivated' : 'keywordListDisplayNotActivated')
    $('#keywordList_activator_ACTIVE').addClass($configuration.KeywordListDisplayType == 'Active' ? 'keywordListDisplayActivated' : 'keywordListDisplayNotActivated')


    collapseWordList($configuration.KeywordListViewCollapse)
    collapseFilterList($configuration.KeywordFilterViewCollapse)

}

// Keyword list sorter where 'key' is either 'Alphabetical' or 'Numerical'
function activateKeywordListSort(key) {
    $configuration.KeywordListSortType = key

    if (key == 'Alphabetical') {
        $keywordList.sort((x, y) => {
            if (x.Content.toLowerCase() > y.Content.toLowerCase()) return 1
            if (x.Content.toLowerCase() < y.Content.toLowerCase()) return -1
            return 0
        })
    } else if (key == 'Numerical') {
        $keywordList.sort((x, y) => y.Count - x.Count)
    }

    loadKeywordList($keywordList)

    if (key == 'Alphabetical') {
        $('#keyword_list__wrapper>div').each((e, word) => {
            let letterNow = $(word).attr('id').split('_')[2]
            let letterBef = $(word).prev().attr('id') ? $(word).prev().attr('id').split('_')[2] : 'Z'
            if (letterBef != letterNow)
                $($('<div/>', {
                        class: 'letter_toggle'
                    }).css({
                        'font-size': '30px',
                        'color': '#212121',
                        'font-weight': '500',
                    }).append($('<span/>').text(letterNow))
                    .append($('<span/>', {
                        class: 'toggle'
                    }).text('▲').css({
                        cursor: 'pointer',
                        float: 'right',
                        'font-size': '20px',
                    }))).insertBefore(word)
        })

        reloadLetterToggles()
    }


    // Letter toggle (letter headers of the keyword list when it is sorted alphabetically) click handler
    $('.letter_toggle>.toggle').click((e) => {
        let letter = $(e.target).prev().text()
        if ($(e.target).text() == '▼') {
            $(e.target).text('▲')
            console.log('.letter_' + letter + ($configuration.KeywordListDisplayType != 'All' ? '.activated' : ''))
            $('.letter_' + letter + ($configuration.KeywordListDisplayType != 'All' ? '.activated' : '')).show('fast')
        } else {
            $(e.target).text('▼')
            $('.letter_' + letter).hide('fast')

        }

        $(e.target).parent().css({
            'font-weight': $(e.target).text() == '▲' ? '500' : 'bolder',
            filter: $(e.target).text() == '▲' ? 'brightness(100%)' : 'brightness(0%)'

        })
    })
}

// Keyword list displayer where 'key' is either 'Active' or 'All'
function activateKeywordListDisplay(key) {
    $configuration.KeywordListDisplayType = key
    key == 'All' ?
        $('.not_activated').not('.letter_toggle').show('fast') :
        $('.not_activated').not('.letter_toggle').hide('fast')

    reloadLetterToggles()
}

// Letter toggles reloader
function reloadLetterToggles() {

    $('.letter_toggle').each((e, el) => {
        $('.letter_' + $(el).children().first().text() + ($configuration.KeywordListDisplayType != 'All' ? '.activated' : '')).length ? $(el).show() : $(el).hide()
    })
}

// Keyword List collapser where 'collapse' is a boolean variable
function collapseWordList(collapse) {
    $configuration.KeywordListViewCollapse = collapse

    $('#keywordList_sorter__wrapper>.toggle').css({
        'background-color': collapse ? '#d8d8d6' : '#545452',
        'color': collapse ? '#545452' : '#d8d8d6'
    })
    collapse ?
        $('#keyword_list__wrapper').hide('fast') :
        $('#keyword_list__wrapper').show('fast')
}

// Filter list collapser where collapse is a boolean variable
function collapseFilterList(collapse) {
    $configuration.KeywordFilterViewCollapse = collapse

    $('#filters__wrapper>.toggle').html(collapse ? '+' : '▲')

    collapse ?
        $('#filters__wrapper>.list').hide('fast') :
        $('#filters__wrapper>.list').show('fast')

    hideTimestampToggle(collapse)
}

// Seconds to Page Time Display converter
function convertSecondsToPageTimeDisplay(seconds) {
    let date = new Date(null)
    date.setSeconds(seconds)
    return date.toISOString().substr(11, 8)
}

// Timestamp click handler
function onWordTimestampClick(word, timestamp) {
    console.log('Running onWordTimestampClick function')
}

// Timestamp toggle
function hideTimestampToggle(yes) {
    $('.timestamp_toggle').each((e, el) => {
        $(el).html(yes ? '▼' : '')
        $(el).css('padding-left', yes ? '5px' : '0')
    })
}